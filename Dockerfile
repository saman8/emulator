FROM openjdk:11
COPY ./build/libs/*.jar emulator.jar
CMD ["java","-jar","emulator.jar"]

#FROM openjdk:11 AS build
#RUN ls -lh
#COPY build.gradle gradlew settings.gradle ./
#COPY gradle/ gradle/
#RUN chmod +x gradlew
#RUN ./gradlew dependencies
#COPY . .
#RUN chmod +x gradlew
#RUN ./gradlew build -x test
#FROM registry.devbourse.com/devops/openjdk:11
#ARG SERVICE_NAME=ZoSo
#ENV SERVICE_NAME=emulator
#ENV HOME=/opt
#ENV APP_HOME=/opt/$SERVICE_NAME
#RUN mkdir $APP_HOME
#COPY --from=build build/libs/*.jar $APP_HOME/
#WORKDIR $APP_HOME


#VOLUME /tmp
#ARG JAR_FILE
#COPY ${JAR_FILE} app.jar
#ENTRYPOINT ["java","-jar","/app.jar"]