package com.egs.atm.emulator.service.impl;

import com.egs.atm.emulator.exception.UserAlreadyExistException;
import com.egs.atm.emulator.model.dto.UserDTO;
import com.egs.atm.emulator.model.entity.AccountEntity;
import com.egs.atm.emulator.model.entity.UserEntity;
import com.egs.atm.emulator.repository.AccountRepository;
import com.egs.atm.emulator.repository.UserRepository;
import com.egs.atm.emulator.service.UserService;
import com.egs.atm.emulator.util.RedisUtil;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.experimental.FieldDefaults;
import lombok.extern.log4j.Log4j;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;

@Service
@AllArgsConstructor
@Slf4j
public class UserServiceImpl implements UserService {

    private final UserRepository userRepository;

    private final AccountRepository accountRepository;

    private final RedisUtil redisUtil;

    private final  PasswordEncoder passwordEncoder;


    @Override
    public UserDTO registerUser(UserDTO userDTO){
        UserEntity user=new UserEntity();
        user.setCardNumber(userDTO.getCardNumber());
        user.setPin(passwordEncoder.encode(userDTO.getPin()));
        AccountEntity accountEntity=new AccountEntity();
        accountEntity.setBalance(BigDecimal.ZERO);
        accountEntity.setUser(user);
        try {
            accountRepository.save(accountEntity);
        }catch (DataIntegrityViolationException e){
            log.error("user already exist",e);
            throw new UserAlreadyExistException();
        }
        return userDTO;
    }

    @Override
    public void failAttempt(String username) {
        Integer failedCount = redisUtil.increaseFailCount(username);
        if (failedCount>=3){
            UserEntity byUsername = userRepository.findByCardNumber(username);
            byUsername.setEnabled(Boolean.FALSE);
            userRepository.save(byUsername);
        }
    }
}
