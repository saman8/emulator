package com.egs.atm.emulator.service;

import com.egs.atm.emulator.model.dto.AccountDTO;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;

public interface AccountService {
    AccountDTO getBalance(String userName);

    @Transactional
    AccountDTO deposit(String userName, BigDecimal value);

    @Transactional
    AccountDTO withdraw(String userName, BigDecimal value);
}
