package com.egs.atm.emulator.service;

import com.egs.atm.emulator.model.dto.UserDTO;
import org.springframework.transaction.annotation.Transactional;

public interface UserService {

    UserDTO registerUser(UserDTO userDTO);

    void failAttempt(String username);
}
