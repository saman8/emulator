package com.egs.atm.emulator.service.impl;

import com.egs.atm.emulator.exception.AccountNotFoundException;
import com.egs.atm.emulator.exception.InsufficientBalanceException;
import com.egs.atm.emulator.model.dto.AccountDTO;
import com.egs.atm.emulator.model.entity.AccountEntity;
import com.egs.atm.emulator.model.mapper.AccountMapper;
import com.egs.atm.emulator.repository.AccountRepository;
import com.egs.atm.emulator.service.AccountService;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;

@Service
@AllArgsConstructor
public class AccountServiceImpl implements AccountService {
    private final AccountRepository accountRepository;
    private final AccountMapper accountMapper;

    @Override
    public AccountDTO getBalance(String userName){
        return accountMapper.toDto(accountRepository.findByUser_CardNumber(userName).orElseThrow(AccountNotFoundException::new));
    }

    @Override
    @Transactional
    public AccountDTO deposit(String userName, BigDecimal value) {
        AccountEntity accountEntity = accountRepository.findByUser_CardNumber((userName)).orElseThrow(AccountNotFoundException::new);
        accountEntity.setBalance(accountEntity.getBalance().add(value));
        return accountMapper.toDto(accountRepository.save(accountEntity));
    }

    @Override
    @Transactional
    public AccountDTO withdraw(String userName, BigDecimal value) {
        AccountEntity accountEntity = accountRepository.findByUser_CardNumber((userName)).orElseThrow(AccountNotFoundException::new);
        if (accountEntity.getBalance().compareTo(value)<0){
            throw new InsufficientBalanceException();
        }
        accountEntity.setBalance(accountEntity.getBalance().subtract(value));
        return accountMapper.toDto(accountRepository.save(accountEntity));
    }
}
