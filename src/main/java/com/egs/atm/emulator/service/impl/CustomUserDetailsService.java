package com.egs.atm.emulator.service.impl;

import com.egs.atm.emulator.exception.TooManyBadCredential;
import com.egs.atm.emulator.model.entity.UserEntity;
import com.egs.atm.emulator.repository.UserRepository;
import lombok.AllArgsConstructor;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;

import java.util.Arrays;
import java.util.Collection;

@Configuration
@AllArgsConstructor
public class CustomUserDetailsService implements UserDetailsService {

//    private PasswordEncoder bcryptEncoder;
	private UserRepository userRepository;

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {

		UserEntity byUsername = userRepository.findByCardNumber(username);
		if (byUsername == null) {
            throw new UsernameNotFoundException("User not found with username: " + username);
        }
		if (!byUsername.getEnabled()){
		    throw new TooManyBadCredential();
        }

            return new User(byUsername.getCardNumber(), byUsername.getPin(),byUsername.getEnabled(),
                    true,true,true,Arrays.asList(new SimpleGrantedAuthority("ROLE_USER")));

    }

}