package com.egs.atm.emulator.repository;

import com.egs.atm.emulator.model.entity.UserEntity;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UserRepository extends CrudRepository<UserEntity,String> {

    UserEntity findByCardNumber(String userName);

}
