package com.egs.atm.emulator.repository;

import com.egs.atm.emulator.model.entity.AccountEntity;
import com.egs.atm.emulator.model.entity.UserEntity;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface AccountRepository extends CrudRepository<AccountEntity,String> {

    Optional<AccountEntity> findByUser_CardNumber(String userName);

}
