package com.egs.atm.emulator.model.entity;

import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "users")
@Data
public class UserEntity {
    @Id
    private String cardNumber;
    private String pin;
    private Boolean enabled=Boolean.TRUE;
}
