package com.egs.atm.emulator.model.dto;

import lombok.Data;

@Data
public class UserDTO {
	private String cardNumber;
	private String pin;

}