package com.egs.atm.emulator.model.mapper;

import com.egs.atm.emulator.model.dto.AccountDTO;
import com.egs.atm.emulator.model.entity.AccountEntity;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper
public interface AccountMapper {
    @Mapping(source = "user.cardNumber",target = "cardNumber")
    AccountDTO toDto(AccountEntity accountEntity);

}
