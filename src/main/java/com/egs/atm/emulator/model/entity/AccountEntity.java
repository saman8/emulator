package com.egs.atm.emulator.model.entity;


import lombok.Data;

import javax.persistence.*;
import java.math.BigDecimal;

@Entity
@Data
public class AccountEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    private BigDecimal balance;

    @OneToOne(cascade=CascadeType.ALL)
    private UserEntity user;

    @Version
    private Long version;


}
