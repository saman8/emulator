package com.egs.atm.emulator.model.dto.request;

import lombok.Data;

import java.math.BigDecimal;
import javax.validation.constraints.Min;

@Data
public class DepositRequestDTO {

    @Min(0)
    private BigDecimal value;
}
