package com.egs.atm.emulator.model.dto;

import lombok.Data;

import java.math.BigDecimal;

@Data
public class AccountDTO {

    private String cardNumber;
    private BigDecimal balance;
}
