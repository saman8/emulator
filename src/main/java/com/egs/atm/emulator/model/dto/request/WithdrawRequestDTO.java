package com.egs.atm.emulator.model.dto.request;

import lombok.Data;

import javax.validation.constraints.Min;
import java.math.BigDecimal;


@Data
public class WithdrawRequestDTO {
    @Min(0)
    private BigDecimal value;
}
