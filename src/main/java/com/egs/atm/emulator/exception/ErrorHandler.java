package com.egs.atm.emulator.exception;

import com.egs.atm.emulator.model.dto.exception.BaseExceptionDTO;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

@ControllerAdvice
public class ErrorHandler {

    @ExceptionHandler(BaseException.class)
    ResponseEntity<BaseExceptionDTO> handleBaseException(BaseException exception){
        return new ResponseEntity(new BaseExceptionDTO(exception.getMessage()), exception.getHttpStatus());
    }
}
