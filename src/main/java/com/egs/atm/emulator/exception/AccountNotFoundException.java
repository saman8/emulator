package com.egs.atm.emulator.exception;

import org.springframework.http.HttpStatus;

public class AccountNotFoundException extends BaseException {
    public AccountNotFoundException() {
        super("account not found", HttpStatus.NOT_FOUND);
    }
}
