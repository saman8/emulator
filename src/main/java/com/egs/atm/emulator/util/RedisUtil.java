package com.egs.atm.emulator.util;

import lombok.AllArgsConstructor;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;

import java.util.Objects;

@Component
@AllArgsConstructor
public class RedisUtil {

    private final RedisTemplate<String,Integer> redisTemplate;


    public Integer increaseFailCount(String username){
        Integer failedCount = redisTemplate.opsForValue().get(username);
        if (Objects.isNull(failedCount)){
            failedCount=0;
        }
        failedCount++;
        redisTemplate.opsForValue().set(username,failedCount);
        return failedCount;
    }

    @Async("redisUtilThreadPool")
    public void clearFailedCount(String username){
        redisTemplate.delete(username);
    }

}
