package com.egs.atm.emulator.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

@Configuration
public class ThreadPoolConfig {

    @Bean
    public ExecutorService redisUtilThreadPool(){
        return Executors.newFixedThreadPool(2);
    }

}
