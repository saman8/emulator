package com.egs.atm.emulator.controller;

import com.egs.atm.emulator.model.dto.AccountDTO;
import com.egs.atm.emulator.model.dto.request.DepositRequestDTO;
import com.egs.atm.emulator.service.AccountService;
import lombok.Data;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.security.Principal;

@RestController
@RequestMapping("/account")
@Data
public class AccountController {

    private final AccountService accountService;
    @GetMapping("/balance")
    @Secured("ROLE_USER")
    public ResponseEntity<AccountDTO> getBalance( Principal principal){
        return new ResponseEntity<>(accountService.getBalance( principal.getName()), HttpStatus.OK);
    }

    @PostMapping("/deposit")
    @Secured("ROLE_USER")
    public ResponseEntity<AccountDTO> deposit(@Valid @RequestBody DepositRequestDTO depositRequestDTO, Principal principal){
        return new ResponseEntity<>(accountService.deposit( principal.getName(), depositRequestDTO.getValue()), HttpStatus.OK);
    }

    @PostMapping("/withdraw")
    @Secured("ROLE_USER")
    public ResponseEntity<AccountDTO> withdraw(@Valid @RequestBody DepositRequestDTO depositRequestDTO, Principal principal){
        return new ResponseEntity<>(accountService.withdraw( principal.getName(), depositRequestDTO.getValue()), HttpStatus.OK);
    }

}
