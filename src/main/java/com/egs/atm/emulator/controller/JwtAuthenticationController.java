package com.egs.atm.emulator.controller;

import com.egs.atm.emulator.config.JwtTokenUtil;
import com.egs.atm.emulator.model.dto.request.JwtRequest;
import com.egs.atm.emulator.service.UserService;
import com.egs.atm.emulator.util.RedisUtil;
import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.DisabledException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;


@RestController
@CrossOrigin
@AllArgsConstructor
public class JwtAuthenticationController {

	private final AuthenticationManager authenticationManager;

	private final JwtTokenUtil jwtTokenUtil;

	private final RedisUtil redisUtil;

	private final UserService userServiceImpl;

	@RequestMapping(value = "/authenticate", method = RequestMethod.POST)
	public ResponseEntity<?> createAuthenticationToken(@RequestBody JwtRequest authenticationRequest) throws Exception {
		final UserDetails userDetails=authenticate(authenticationRequest.getUsername(), authenticationRequest.getPassword());
		final String token = jwtTokenUtil.generateToken(userDetails);
		redisUtil.clearFailedCount(userDetails.getUsername());
		return ResponseEntity.ok(new JwtResponse(token));
	}



	private UserDetails authenticate(String username, String password) throws Exception {
		try {
			Authentication authenticate = authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(username, password));

			return new User(((User)authenticate.getPrincipal()).getUsername(),"",
					((User)authenticate.getPrincipal()).getAuthorities());
		} catch (DisabledException e) {
			throw new Exception("USER_DISABLED", e);
		} catch (BadCredentialsException e) {
			userServiceImpl.failAttempt(username);
			throw new Exception("INVALID_CREDENTIALS", e);
		}
	}



	private class JwtResponse {
		private String token;
		public JwtResponse(String token) {
			this.token=token;
		}

		public String getToken() {
			return token;
		}

		public void setToken(String token) {
			this.token = token;
		}
	}
}