package com.egs.atm.emulator.controller;


import com.egs.atm.emulator.model.dto.UserDTO;
import com.egs.atm.emulator.service.UserService;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/user")
@AllArgsConstructor
public class UserController {


    private final UserService userServiceImpl;

    @PostMapping
    public void registerUser(@RequestBody UserDTO user) {
        userServiceImpl.registerUser(user);
    }
}
