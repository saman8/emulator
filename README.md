[![Build Status](https://travis-ci.org/ExampleDriven/spring-boot-docker-example.svg?branch=master)](https://travis-ci.org/ExampleDriven/spring-boot-docker-example)


How to run this example :

## build docker images
./gradlew build
docker  build -t emulator:1.0.0 .

##should display three freshly built docker images
docker images

##start up all instances
docker-compose up